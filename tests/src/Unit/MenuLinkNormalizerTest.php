<?php

namespace Drupal\Tests\menu_normalizer\Unit;

use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\menu_normalizer\Normalizer\MenuLinkNormalizer;
use Drupal\Tests\Core\Menu\MenuLinkMock;

/**
 * @coversDefaultClass \Drupal\menu_normalizer\Normalizer\MenuLinkNormalizer
 * @group menu_normalizer
 */
class MenuLinkNormalizerTest extends MenuNormalizerTestBase {

  /**
   * Menu link normalizer.
   *
   * @var \Drupal\menu_normalizer\Normalizer\MenuLinkNormalizer
   */
  protected $menuLinkNormalizer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->menuLinkNormalizer = new MenuLinkNormalizer();
    $this->menuLinkNormalizer->setSerializer($this->serializer);
  }

  /**
   * @covers ::supportsNormalization
   */
  public function testSupportsNormalization() {
    $menu_link_mock = $this->createMock(MenuLinkInterface::class);
    $this->assertTrue($this->menuLinkNormalizer->supportsNormalization($menu_link_mock));
  }

  /**
   * @covers ::normalize
   */
  public function testNormalize() {
    $definition = [
      'id' => 'test.example1',
      'route_name' => 'example1',
      'title' => 'foo',
      'parent' => '',
      'weight' => 0
    ];
    $menu_link = MenuLinkMock::create($definition);
    $normalized = $this->menuLinkNormalizer->normalize($menu_link);
    $this->assertIsArray($normalized);
    foreach ($definition as $key => $value) {
      $this->assertArrayHasKey($key, $normalized);
      $this->assertEquals($value, $normalized[$key]);
    }
  }

}
