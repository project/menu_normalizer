<?php

namespace Drupal\Tests\menu_normalizer\Unit;

use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\menu_normalizer\Normalizer\MenuLinkNormalizer;
use Drupal\menu_normalizer\Normalizer\MenuLinkTreeNormalizer;
use Drupal\Tests\Core\Menu\MenuLinkMock;

/**
 * @coversDefaultClass \Drupal\menu_normalizer\Normalizer\MenuLinkTreeNormalizer
 * @group menu_normalizer
 */
class MenuLinkTreeNormalizerTest extends MenuNormalizerTestBase {

  /**
   * Menu link tree normalizer.
   *
   * @var \Drupal\menu_normalizer\Normalizer\MenuLinkTreeNormalizer
   */
  protected $menuLinkTreeNormalizer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->menuLinkTreeNormalizer = new MenuLinkTreeNormalizer();
    $this->menuLinkTreeNormalizer->setSerializer($this->serializer);
  }

  /**
   * @covers ::supportsNormalization
   */
  public function testSupportsNormalization() {
    $menu_link_tree_mock = $this->createMock(MenuLinkTreeElement::class);
    $this->assertTrue($this->menuLinkTreeNormalizer->supportsNormalization($menu_link_tree_mock));
  }

  /**
   * @covers ::normalize
   */
  public function testNormalize() {
    $link_1 = MenuLinkMock::create(['id' => 'test_1']);
    $link_2 = MenuLinkMock::create(['id' => 'test_2']);
    $child_item = new MenuLinkTreeElement($link_2, FALSE, 3, FALSE, []);
    $parent_item = new MenuLinkTreeElement($link_1, TRUE, 2, FALSE, [$child_item]);
    $child_normalized = $this->menuLinkTreeNormalizer->normalize($child_item);
    $parent_normalized = $this->menuLinkTreeNormalizer->normalize($parent_item);

    $this->assertIsArray($child_normalized);
    $this->assertIsArray($parent_normalized);

    $this->assertArrayHasKey('has_children', $child_normalized);
    $this->assertFalse($child_normalized['has_children']);
    $this->assertArrayHasKey('has_children', $parent_normalized);
    $this->assertTrue($parent_normalized['has_children']);

    $this->assertArrayHasKey('count', $child_normalized);
    $this->assertSame(1, $child_normalized['count']);
    $this->assertArrayHasKey('count', $parent_normalized);
    $this->assertSame(2, $parent_normalized['count']);
  }

}
