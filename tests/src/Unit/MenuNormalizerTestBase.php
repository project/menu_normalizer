<?php

namespace Drupal\Tests\menu_normalizer\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Routing\UrlGenerator;
use Drupal\menu_normalizer\Normalizer\MenuLinkNormalizer;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Serializer\Serializer;

/**
 * Defines a base unit test for testing normalizers.
 */
abstract class MenuNormalizerTestBase extends UnitTestCase {

  /**
   * The mock serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $serializer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->serializer = $this->getMockBuilder(Serializer::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['normalize'])
      ->getMock();
    $url_generator = $this->getMockBuilder(UrlGenerator::class)
      ->disableOriginalConstructor()
      ->getMock();
    $container = new ContainerBuilder();
    $container->set('url_generator', $url_generator);
    \Drupal::setContainer($container);
  }

}
